const Truck = require('../models/truck-model');

const getTrucks = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'DRIVER') {
      throw new Error(`${role} can not get trucks`);
    }

    const trucks = await Truck.find(
        {created_by: id},
        {
          __v: 0,
        },
    );

    res.status(200).json({trucks});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const addTruck = async (req, res) => {
  try {
    const {id, role, type} = req.body;

    if (role != 'DRIVER') {
      throw new Error(`${role} can not add trucks`);
    }
    if (!type) {
      throw new Error('no type');
    }

    const truck = new Truck({
      created_by: id,
      assigned_to: '',
      type,
      status: 'IS',
      created_date: new Date(),
    });
    await truck.save();

    res.status(200).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const getTruck = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'DRIVER') {
      throw new Error(`${role} can not get trucks`);
    }

    const truck = await Truck.findById(req.params.id, {
      __v: 0,
    });
    if (!truck) {
      throw new Error('truck is not found');
    }
    if (truck.created_by != id) {
      throw new Error('DRIVER do not have this truck');
    }

    res.status(200).json({truck});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const updateTruck = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'DRIVER') {
      throw new Error(`${role} can not update trucks`);
    }

    const truck = await Truck.findById(req.params.id);
    if (!truck) {
      throw new Error('truck is not found');
    }
    if (truck.created_by != id) {
      throw new Error('DRIVER do not have this truck');
    }
    const trucks = await Truck.find({created_by: id, status: 'OL'});
    if (trucks.length > 0) {
      throw new Error(`driver can not update truck info
       when he is not ON LOAD`);
    }

    if (!req.body.type) {
      throw new Error('truck type required');
    }

    await Truck.findByIdAndUpdate(req.params.id, {type: req.body.type});
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const deleteTruck = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'DRIVER') {
      throw new Error(`${role} can not delete trucks`);
    }

    const truck = await Truck.findById(req.params.id);
    if (!truck) {
      throw new Error('truck is not found');
    }
    if (truck.created_by != id) {
      throw new Error('DRIVER do not have this truck');
    }
    const trucks = await Truck.find({created_by: id, status: 'OL'});
    if (trucks.length > 0) {
      throw new Error(
          'driver can not update truck info when he is not ON LOAD',
      );
    }

    await Truck.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const assignTruck = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'DRIVER') {
      throw new Error(`${role} can not assign trucks`);
    }

    const truck = await Truck.findById(req.params.id, {
      __v: 0,
    });
    if (!truck) {
      throw new Error('truck is not found');
    }
    if (truck.created_by != id) {
      throw new Error('DRIVER do not have this truck');
    }

    const trucks = await Truck.find({assigned_to: id});
    if (trucks.length > 0) {
      throw new Error('driver can assign only one truck');
    }

    await Truck.findByIdAndUpdate(req.params.id, {assigned_to: id});

    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
