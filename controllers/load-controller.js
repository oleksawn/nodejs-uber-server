const Load = require('../models/load-model');
const Truck = require('../models/truck-model');

const getLoads = async (req, res) => {
  try {
    const {id, role} = req.body;
    const offset = Number(req.query.offset) || 0;
    const limit =
      Number(req.query.limit) > 50 ? 50 : Number(req.query.limit) || 10;
    const status = req.query.status;

    if (
      status &&
      status !== 'NEW' &&
      status !== 'POSTED' &&
      status !== 'ASSIGNED' &&
      status !== 'SHIPPED'
    ) {
      throw new Error('this status does not exist');
    }

    if (role == 'SHIPPER') {
      let loads = [];
      if (status) {
        loads = await Load.find(
            {created_by: id, status},
            {
              __v: 0,
            },
        )
            .skip(offset)
            .limit(limit);
      } else {
        loads = await Load.find(
            {created_by: id},
            {
              __v: 0,
            },
        )
            .skip(offset)
            .limit(limit);
      }

      res.status(200).json({loads});
    } else if (role == 'DRIVER') {
      let loads = [];
      if (status) {
        if (status !== 'ASSIGNED' && status !== 'SHIPPED') {
          throw new Error(
              'driver can get loads just with assigned or shipped status',
          );
        }
        loads = await Load.find(
            {assigned_to: id, status},
            {
              __v: 0,
            },
        )
            .skip(offset)
            .limit(limit);
      } else {
        loads = await Load.find(
            {assigned_to: id, status: /(ASSIGNED)||(SHIPPED)/},
            {
              __v: 0,
            },
        )
            .skip(offset)
            .limit(limit);
      }

      res.status(200).json({loads});
    }
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const addLoad = async (req, res) => {
  try {
    const {
      id,
      role,
      name,
      payload,
      pickup_address, // eslint-disable-line camelcase
      delivery_address, // eslint-disable-line camelcase
      dimensions,
    } = req.body;

    if (role != 'SHIPPER') {
      throw new Error(`${role} can not add trucks`);
    }

    if (
      !name ||
      !payload ||
      !pickup_address || // eslint-disable-line camelcase
      !delivery_address || // eslint-disable-line camelcase
      !dimensions.length ||
      !dimensions.width ||
      !dimensions.height
    ) {
      throw new Error('all fields should be filled');
    }

    const load = new Load({
      created_by: id,
      assigned_to: '',
      status: 'NEW',
      state: '',
      name,
      payload,
      pickup_address, // eslint-disable-line camelcase
      delivery_address, // eslint-disable-line camelcase
      dimensions,
      logs: [],
      created_date: new Date(),
    });
    await load.save();

    res.status(200).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const getActiveLoad = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role !== 'DRIVER') {
      throw new Error(`${role} can not get active load`);
    }
    const [load] = await Load.find(
        {assigned_to: id, status: 'ASSIGNED'},
        {
          __v: 0,
        },
    );
    res.status(200).json({load});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const nextActiveLoadState = async (req, res) => {
  try {
    const {id, role} = req.body;
    const states = [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ];
    if (role !== 'DRIVER') {
      throw new Error(`${role} can not get active load`);
    }
    const [load] = await Load.find(
        {assigned_to: id, status: 'ASSIGNED'},
        {
          __v: 0,
        },
    );
    if (!load) {
      throw new Error('no active load');
    }

    const indexOfCurrentLoadState = states.findIndex((el) => el == load.state);
    if (indexOfCurrentLoadState == -1) {
      throw new Error('wrong load state');
    } else if (indexOfCurrentLoadState == states.length - 1) {
      throw new Error(`${load.state} is the last state`);
    } else {
      await Load.findByIdAndUpdate(load._id, {
        state: states[indexOfCurrentLoadState + 1],
        $push: {
          logs: {
            messgae: `load get state ${states[indexOfCurrentLoadState + 1]}`,
            time: new Date(),
          },
        },
      });
      if (indexOfCurrentLoadState == states.length - 2) {
        await Load.findByIdAndUpdate(load._id, {
          status: 'SHIPPED',
        });
        const [truck] = await Truck.find({assigned_to: id, status: 'OL'});
        await Truck.findByIdAndUpdate(truck._id, {status: 'IS'});
      }
    }
    res.status(200).json({
      message: `Load state changed to ${states[indexOfCurrentLoadState + 1]}`,
    });
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const getLoad = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'SHIPPER') {
      throw new Error(`${role} can not get loads`);
    }

    const load = await Load.findById(req.params.id, {
      __v: 0,
    });
    if (load.created_by != id) {
      throw new Error('SHIPPER do not have this load');
    }

    res.status(200).json({load});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const updateLoad = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'SHIPPER') {
      throw new Error(`${role} can not get loads`);
    }

    const load = await Load.findById(req.params.id, {
      __v: 0,
    });
    if (load.created_by != id) {
      throw new Error('SHIPPER do not have this load');
    }
    if (load.status != 'NEW') {
      throw new Error('SHIPPER can update just NEW loads');
    }

    const fieldsForUpdate = Object.keys(req.body);
    const updatedData = {};
    const dimensionsData = {};
    fieldsForUpdate
        .filter((field) => {
          return (
            field == 'name' ||
          field == 'payload' ||
          field == 'pickup_address' ||
          field == 'delivery_address' ||
          field == 'dimensions'
          );
        })
        .forEach((field) => {
          if (field == 'dimensions') {
            if (typeof req.body.dimensions == 'object') {
              const nestedFieldsForUpdate = Object.keys(req.body.dimensions);
              nestedFieldsForUpdate
                  .filter((nestedField) => {
                    return (
                      nestedField == 'width' ||
                  nestedField == 'length' ||
                  nestedField == 'height'
                    );
                  })
                  .forEach((nestedField) => {
                    dimensionsData['dimensions.' + nestedField] =
                  req.body.dimensions[nestedField];
                  });
            }
            updatedData['$set'] = dimensionsData;
          } else {
            updatedData[field] = req.body[field];
          }
        });

    await Load.findByIdAndUpdate(req.params.id, updatedData);

    res.status(200).json({message: 'Load details changed successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const deleteLoad = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role != 'SHIPPER') {
      throw new Error(`${role} can not get loads`);
    }

    const load = await Load.findById(req.params.id, {
      __v: 0,
    });
    if (load.created_by != id) {
      throw new Error('SHIPPER do not have this load');
    }
    if (load.status != 'NEW') {
      throw new Error('SHIPPER can delete just NEW loads');
    }

    await Load.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const postLoad = async (req, res) => {
  try {
    const {id, role} = req.body;
    const truckCapacity = {
      'SPRINTER': {
        payload: 1700,
        length: 300,
        width: 250,
        height: 170,
      },
      'SMALL STRAIGHT': {
        payload: 2500,
        length: 500,
        width: 250,
        height: 170,
      },
      'LARGE STRAIGHT': {
        payload: 4000,
        length: 700,
        width: 350,
        height: 200,
      },
    };

    if (role != 'SHIPPER') {
      throw new Error(`${role} can not get loads`);
    }

    const load = await Load.findById(req.params.id);
    if (load.created_by != id) {
      throw new Error('SHIPPER do not have this load');
    }
    if (load.status != 'NEW') {
      throw new Error('SHIPPER can post just NEW loads');
    }

    await Load.findByIdAndUpdate(req.params.id, {status: 'POSTED'});

    const trucks = await Truck.find({status: 'IS'});
    let availableTruckId = '';
    trucks.forEach((truck) => {
      if (
        truck.assigned_to &&
        load.payload < truckCapacity[truck.type].payload &&
        load.dimensions.length < truckCapacity[truck.type].length &&
        load.dimensions.width < truckCapacity[truck.type].width &&
        load.dimensions.height < truckCapacity[truck.type].height
      ) {
        availableTruckId = truck._id;
      }
    });

    if (!availableTruckId) {
      await Load.findByIdAndUpdate(req.params.id, {
        status: 'NEW',
        $push: {
          logs: {
            message: 'No driver found, load return NEW status',
            time: new Date(),
          },
        },
      });

      res.status(200).json({
        message: 'Load posted, no available truck',
        driver_found: false,
      });
    } else {
      await Truck.findByIdAndUpdate(availableTruckId, {status: 'OL'});
      const avaliabletruck = await Truck.findById(availableTruckId);
      await Load.findByIdAndUpdate(req.params.id, {
        assigned_to: avaliabletruck.assigned_to,
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        $push: {
          logs: {
            message: `load assigned to driver ${avaliabletruck.assigned_to}, 
            get state En route to Pick Up`,
            time: new Date(),
          },
        },
      });

      res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const getLoadInfo = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role !== 'SHIPPER') {
      throw new Error(`${role} can not get shipping info`);
    }

    const load = await Load.findById(req.params.id, {
      __v: 0,
    });
    if (load.created_by != id) {
      throw new Error('SHIPPER do not have this load');
    }
    if (!load.assigned_to || load.status !== 'ASSIGNED') {
      throw new Error('load does not have ASSIGNED status');
    }

    res.status(200).json({load});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  nextActiveLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
};
