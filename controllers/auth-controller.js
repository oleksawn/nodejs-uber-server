const bcrypt = require('bcryptjs');
const User = require('../models/user-model');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
  try {
    const {email, password, role} = req.body;

    const userFromDb = await User.findOne({email});
    if (userFromDb) {
      throw new Error(`${email} already exist`);
    }

    const user = new User({
      email,
      password: bcrypt.hashSync(password, 8),
      role,
      created_date: new Date(),
    });
    await user.save();

    return res.status(200).json({message: 'Profile created successfully'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const login = async (req, res) => {
  try {
    const {email, password} = req.body;
    if (!email) {
      throw new Error('email is empty');
    }
    if (!password) {
      throw new Error('password is empty');
    }

    const userFromDb = await User.findOne({email});
    if (!userFromDb) {
      throw new Error(`${email} does not exist`);
    }

    const isPasswordVerified = bcrypt.compareSync(
        password,
        userFromDb.password,
    );
    if (!isPasswordVerified) {
      throw new Error('wrong password');
    }

    const token = jwt.sign(
        {id: userFromDb._id, role: userFromDb.role},
        process.env.JWT_KEY,
        {
          expiresIn: '24h',
        },
    );
    return res.status(200).json({jwt_token: token});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const forgotPassword = async (req, res) => {
  try {
    const {email} = req.body;

    const userFromDb = await User.findOne({email});
    if (!userFromDb) {
      throw new Error(`${email} does not exist`);
    }

    // generate new password !
    // send message to emeil!!!

    return res
        .status(200)
        .json({message: 'New password sent to your email address'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports = {
  register,
  login,
  forgotPassword,
};
