const User = require('../models/user-model');
const Truck = require('../models/truck-model');
const bcrypt = require('bcryptjs');

const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.body.id, {password: 0, __v: 0});
    res.status(200).json({user});
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    const {id, role} = req.body;
    if (role == 'DRIVER') {
      const trucks = await Truck.find({created_by: id, status: 'OL'});
      if (trucks.length > 0) {
        throw new Error('driver can not delete profile when he is not ON LOAD');
      }
    }

    await User.findByIdAndDelete(req.body.id);
    res.status(200).json({message: `Profile deleted successfully`});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const changeUserPassword = async (req, res) => {
  try {
    const {id, oldPassword, newPassword} = req.body;

    if (oldPassword == newPassword) {
      throw new Error('you can not use old password');
    }

    const user = await User.findById(id);

    const isPasswordVerified = bcrypt.compareSync(oldPassword, user.password);
    if (!isPasswordVerified) {
      throw new Error('wrong password');
    }

    await User.findByIdAndUpdate(req.body.id, {
      password: bcrypt.hashSync(newPassword, 8),
    });

    res.status(200).json({message: `Password changed successfully`});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
