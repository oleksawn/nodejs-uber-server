const validEmail = (req, res, next) => {
  try {
    if (!req.body.email) {
      throw new Error('no email');
    } else if (
      !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i
          .test(
              req.body.email,
          )
    ) {
      throw new Error(`email is not valid`);
    }
    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const validPassword = (req, res, next) => {
  try {
    password = req.body.password || req.body.newPassword;
    if (!password) {
      throw new Error('no password');
    } else if (
      !/^[a-z0-9\.\,\_\-\+\!\?\:\;\`\'\"\*\^\~\@\$\%\&\#\(\)]{2,60}$/i.test(
          password,
      )
    ) {
      throw new Error(
          `password must consist of 2 up to 60, 
        and can contain latin letters, digits 
        and characters .,_-+!?:;\`'"*^~@$%&#()`,
      );
    }
    /*
  else if (!/[a-z]{1,}/.test(req.body.password)) {
    throw new Error(
      'password must contain at least one lowercase letter'
    );
  } else if (!/[A-Z]{1,}/.test(req.body.password)) {
    throw new Error(
      'password must contain at least one uppercase letter'
    );
  } else if (!/[0-9]{1,}/.test(req.body.password)) {
    throw new Error('password must contain at least one digit');
  } else if (
    !/[\.\,\_\-\+\!\?\:\;\`\'\"\*\^\~\@\$\%\&\#\|\\\/\(\)\{\}\[\]\<\>]{1,}/
    .test(
      req.body.password
    )
  ) {
    throw new Error(
      'password must contain at least one character'
    );
  }
  */
    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

const validRole = (req, res, next) => {
  try {
    if (!req.body.role) {
      throw new Error('no role');
    } else if (req.body.role != 'SHIPPER' && req.body.role != 'DRIVER') {
      throw new Error('this role does not exist');
    }
    next();
  } catch (err) {
    res.status(400).json({
      message: err.message,
    });
  }
};

module.exports = {
  validEmail,
  validPassword,
  validRole,
};
