const express = require('express');
const loadRouter = express.Router(); // eslint-disable-line new-cap
const {
  getLoads,
  addLoad,
  getActiveLoad,
  nextActiveLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
} = require('../controllers/load-controller');

loadRouter.get('/', getLoads);
loadRouter.post('/', addLoad);
loadRouter.get('/active', getActiveLoad);
loadRouter.patch('/active/state', nextActiveLoadState);
loadRouter.get('/:id', getLoad);
loadRouter.put('/:id', updateLoad);
loadRouter.delete('/:id', deleteLoad);
loadRouter.post('/:id/post', postLoad);
loadRouter.get('/:id/shipping_info', getLoadInfo);

module.exports = loadRouter;
