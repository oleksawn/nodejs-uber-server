const express = require('express');
const authRouter = express.Router(); // eslint-disable-line new-cap
const {
  register,
  login,
  forgotPassword,
} = require('../controllers/auth-controller');
const {
  validEmail,
  validPassword,
  validRole,
} = require('../middleware/valid-field');

authRouter.post('/register', validEmail, validPassword, validRole, register);
authRouter.post('/login', login);
authRouter.post('/forgot_password', forgotPassword);

module.exports = authRouter;
