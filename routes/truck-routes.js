const express = require('express');
const truckRouter = express.Router(); // eslint-disable-line new-cap
const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/truck-controller');

truckRouter.get('/', getTrucks);
truckRouter.post('/', addTruck);
truckRouter.get('/:id', getTruck);
truckRouter.put('/:id', updateTruck);
truckRouter.delete('/:id', deleteTruck);
truckRouter.post('/:id/assign', assignTruck);

module.exports = truckRouter;
