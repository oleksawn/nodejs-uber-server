require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const {urlencoded} = require('express');
const cors = require('cors');
const morgan = require('morgan');

const authRouter = require('./routes/auth-routes');
const userRouter = require('./routes/user-routes');
const truckRouter = require('./routes/truck-routes');
const loadRouter = require('./routes/load-routes');

const authMiddleware = require('./middleware/get-auth-token');

const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors());
app.use(urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('dev'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);

mongoose
    .connect(
        process.env.MONGO_URL,
        {useNewUrlParser: true},
        {useUnifiedTopology: true},
    )
    .then(() => {
      console.log('mongo db connected');
      app.listen(PORT, () => {
        console.log(`server listen on port ${PORT}`);
      });
    })
    .catch((err) => {
      console.log('mongo db error', err);
    });
