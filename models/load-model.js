const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loadSchema = new Schema({
  created_by: {
    type: String,
    require: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true,
  },
  state: {
    type: String,
    enum: [
      '',
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Object,
    Array: {
      message: {
        type: String,
      }, // Load assigned to driver with id ###
      time: {
        type: Date,
      },
    },
  },
  created_date: {
    type: Date,
  },
});

const Load = mongoose.model('Load', loadSchema);

module.exports = Load;
