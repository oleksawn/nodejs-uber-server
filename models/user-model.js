const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
  },
  created_date: {
    type: Date,
  },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
